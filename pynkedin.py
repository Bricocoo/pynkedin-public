# Avant tout ça : 
# python3.10 -m venv pynkedin
# source pynkedin/bin/activate
# pip install selenium pandas

from os import path, makedirs
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import os
import pandas as pd
import random
import re
import time

####################################################################################################################
## Variables globales
####################################################################################################################
# Remplir username et password avec identifiants LinkedIn

username = ""
password = ""


indies = pd.read_csv("")
# indies["nom_complet"] = [str(indies["prenom"][x]) + " " + str(indies["nom_famille"][x]) for x in range(len(indies))]
indies["nom_complet"] = [str(indies["nom_complet"][x]) for x in range(len(indies))]

# Les mots clés à chercher dans la rubrique "Infos" du profil
mots_cles = ""

dir_save = ""
## Traitement des variables globales
re_mots_cles = re.compile(mots_cles, re.IGNORECASE)

dir_profiles = path.join(path.expanduser(dir_save), "results/profils/")
dir_experiences = path.join(path.expanduser(dir_save), "results/experiences/")
dir_formation = path.join(path.expanduser(dir_save), "results/formation/")
for dapath in [dir_profiles, dir_experiences, dir_formation]:
    if not path.exists(dapath):
        makedirs(dapath)

file_done = path.join(path.expanduser(dir_save), "scraping-done.txt")

####################################################################################################################
## Fonctions
####################################################################################################################


def random_sleep(a=1.0, b=2.0):
    time.sleep(random.uniform(a, b))


def slow_fill(element, txt):
    for i in range(len(txt)):
        random_sleep(.1, .3)
        element.send_keys(txt[i])


def wait_until_visible(bro, xpath, text="", seconds=1):
    if not bro.find_elements('xpath', xpath).__len__():
        print("** Wait until loaded " + text + "...")
    while not bro.find_elements('xpath', xpath).__len__():
        time.sleep(seconds)


def wait_while_visible(bro, xpath, text="", seconds=1):
    if bro.find_elements('xpath', xpath).__len__():
        print("** Wait while loading " + text + "...")
    while bro.find_elements('xpath', xpath).__len__():
        time.sleep(seconds)


def write_done(name):
    with open(file_done, "a") as dafile:
        dafile.writelines(name + "\n")

####################################################################################################################
## Start driver, connect to page
####################################################################################################################

## Chargement des déjà faits
if not path.exists(file_done):
    is_done = []
else:
    with open(file_done, "r") as dafile:
        is_done = dafile.readlines()


## Ouverture browser


from selenium.webdriver.firefox.options import Options
options = Options()
install_dir = "/opt/"
geckodriver_path = '/opt/geckodriver'
# driver = webdriver.Firefox(executable_path=geckodriver_path, options=options)



# install_dir = "/usr/local/bin/"


# driver_loc = "/usr/local/bin/geckodriver"
# driver_loc = os.path.join(install_dir, "geckodriver")
# binary_loc = os.path.join(install_dir, "firefox")
# service = webdriver.FirefoxProfile(driver_loc)
# opts = webdriver.FirefoxOptions()
# opts.binary_location = binary_loc
# driver = webdriver.Firefox(service=service, options=opts)


driver = webdriver.Firefox()


## Login
driver.get("https://www.linkedin.com/login")
random_sleep(2, 4)
wait_until_visible(driver, '//input[@id="username"]', "login page")
elt = driver.find_element("xpath", "//input[@id='username']")
elt.click()
elt.send_keys(username)
elt = driver.find_element("xpath", "//input[@id='password']")
elt.click()
elt.send_keys(password)
driver.find_element("css selector", "button[type='submit']").click()
wait_until_visible(driver, '//div[@id="global-nav-search"]', "main page")


## Boucle principale

for i in range(1,len(indies)):
# for i in range(len(indies)):

    daname = indies.nom_complet[i]
    if (len(str(indies.organisation[i]))):
        daname = daname + " " + "sustainable finance"

    # Flag pour savoir si la recherche initiale a échoué
    initial_search_failed = False # MODIFICATION
    
    ## On ne scrape que si pas déjà tenté
    # if (daname) in is_done:  
        
    is_present = any(indies.nom_complet[i] in s for s in is_done)
    if (is_present):
        print("Déjà fait : " + daname)
        continue

    print("\n" + 10 * "*" + "\nRecherche : " + daname)

    ## Search name with org
    elt = driver.find_element("css selector", ".search-global-typeahead__input")
    elt.clear()
    slow_fill(elt, daname, )
            
    elt.send_keys(Keys.ENTER)
    wait_until_visible(driver, '//div[@class="search-results-container"]', "search results")
    

    ## Limit to persons
    if not "/results/people/" in driver.current_url:
        elts = driver.find_elements('xpath', '//button[text()="Personnes"]')
        if len(elts):
            elts[0].click()
        else:
            driver.find_element('xpath', '//li[@class="search-reusables__primary-filter"]').click()
            wait_until_visible(driver, '//li[@class="search-reusables__primary-filter"]//*[text()="Personnes"]', "menu item Persons")
            driver.find_element('xpath', '//li[@class="search-reusables__primary-filter"]//*[text()="Personnes"]').click()

    
    ## No result?
    no_result = driver.find_elements('css selector', '.artdeco-empty-state__message')
    if (len(no_result)):
        print("Aucun résultat trouvé pour " + daname + " - passage au suivant.")
        initial_search_failed = True # MODIFICATION
        
        
    if initial_search_failed: # MODIFICATION
        # Recherche uniquement avec le nom de l'individu
        initial_search_failed = False
        elt = driver.find_element("css selector", ".search-global-typeahead__input")
        elt.clear()
        slow_fill(elt, indies.nom_complet[i]) # MODIFICATION : Utilisation du nom seul
        elt.send_keys(Keys.ENTER)
        wait_until_visible(driver, '//div[@class="search-results-container"]', "search results")
        # Vérifiez à nouveau s'il y a des résultats
        no_result = driver.find_elements('css selector', '.artdeco-empty-state__message')
        if (len(no_result)):
            print("Aucun résultat trouvé pour " + indies.nom_complet[i] + " - passage au suivant.") # MODIFICATION
            write_done(daname) # MODIFICATION : Sauvegarde du nom utilisé pour la recherche
            continue    
        
    
    random_sleep(1, 4)
    if not initial_search_failed or (initial_search_failed and len(no_result) == 0):
        found = False
        primary_subtitles = driver.find_elements("css selector", ".entity-result__primary-subtitle.t-14.t-black.t-normal")
        for primsub in primary_subtitles:
            if re.search(re_mots_cles, primsub.text):
                print("Mots clé trouvés dans 1st sub")
                found = True
                primsub.click()

        if not found:
            third_subtitles = driver.find_elements("css selector", ".reusable-search-simple-insight")
            for thirdsub in third_subtitles:
                if re.search(re_mots_cles, thirdsub.text):
                    print("Mots clé trouvés dans 3d sub"); found = True
                    thirdsub.click()

        if not found:
            profile_count = driver.find_elements('css selector', '.entity-result__title-text.t-16')
            if len(profile_count) == 1:
                print("Un seul profil pour " + daname + " -> On prend !")
                found = True
                profile_count[0].click()

        if not found:
            print("Aucun bon résultat trouvé pour " + daname + " - passage au suivant.")
            write_done(daname)
            continue

    wait_until_visible(driver, '//span[text()="Activité"]', "profile")

    ## Sauvegarde profil
    file_profile = path.join(dir_profiles, daname + ".html")
    with open(file_profile, "w") as dafile:
        dafile.writelines(driver.page_source)
    random_sleep(1, 2)

    ## Sauvegarde experience plus
    elts = driver.find_elements("css selector", ".pvs-navigation__text")
    elts = [elt for elt in elts if re.search("expérience.(?! de bénévolat)", elt.text)]
    if len(elts):
        elts[-1].click()
        wait_until_visible(driver, '//h2[text()="Expérience"]', "experience")
        file_xp = path.join(dir_experiences, daname + ".html")
        with open(file_xp, "w") as dafile:
            dafile.writelines(driver.page_source)
        driver.back()
        print("Expérience+ sauvegardée pour " + daname)
        wait_until_visible(driver, '//span[text()="Activité"]', "profile")
    else:
        print("Pas d'expérience+")
    random_sleep(1, 2)

    ## Sauvegarde formation plus
    elts = driver.find_elements("css selector", ".pvs-navigation__text")
    elts = [elt for elt in elts if re.search("formation", elt.text)]
    if len(elts):
        elts[-1].click()
        wait_until_visible(driver, '//h2[text()="Formation"]', "formation")
        file_formation = path.join(dir_formation, daname + ".html")
        with open(file_formation, "w") as dafile:
            dafile.writelines(driver.page_source)
        driver.back()
        print("Formation+ sauvegardée pour " + daname)
        wait_until_visible(driver, '//span[text()="Activité"]', "profile")
    else:
        print("Pas de formation+")
    random_sleep(1, 2)

    write_done(daname)



